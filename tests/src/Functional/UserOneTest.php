<?php

namespace Drupal\Tests\userone\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Class UserOneTest. The base class for testing userone features.
 */
class UserOneTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'userone'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test userone features.
   */
  public function testUserOne() {
    // Log in as an admin user.
    $admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($admin);

    // The user should not be the user one.
    $this->assertNotEquals(1, $admin->id());

    // The user should not have access to user one.
    $this->drupalGet('user/1');
    $this->assertSession()->statusCodeEquals(403);

    // The user should not see user one on the page.
    $this->drupalGet('admin/people');
    $this->assertSession()->elementTextNotContains('css', 'table tr td a', 'admin');

    $this->drupalLogout();

    // Load user one and set a new password.
    $one = User::load(1);
    $one->setPassword('pass');
    $one->save();

    $one->passRaw = 'pass';
    $this->drupalLogin($one);

    // The user should have access to user one.
    $this->drupalGet('user/1');
    $this->assertSession()->statusCodeEquals(200);

    // The user should have access to user one.
    $this->drupalGet('admin/people');
    $this->assertSession()->elementTextContains('css', 'table tr td a', 'admin');
  }

}
